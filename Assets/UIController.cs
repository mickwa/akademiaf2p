﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{ 
    public static UIController Instance = null;
    public FinishedLevelPanel finishedLevelPanel;

    void Awake()
    {
    if (Instance == null)
        Instance = this;
    }
    public void ShowFinishedLevelPanel()
    {
        finishedLevelPanel.Activate();
    }
    public void ShowLostLevelPanel()
    {
        finishedLevelPanel.ActivateKill();
    }
    public void TutorialNextStep()
    {
        GetComponentInChildren<StartGameCountDown>().ShowTapInfo();
    }
    public void TutorialEnd()
    {
        GetComponentInChildren<StartGameCountDown>().TutorialShow();
    }
    public void hideTutorialInfo() {
        GetComponentInChildren<StartGameCountDown>().CounterText.enabled = false;
    }

}