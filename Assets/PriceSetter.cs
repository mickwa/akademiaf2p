﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class PriceSetter : MonoBehaviour
{

  public Card c;
  void Start()
  {
    GetComponent<TextMeshProUGUI>().text = c.cost.ToString();
  }
}