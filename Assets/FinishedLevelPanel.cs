﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FinishedLevelPanel : MonoBehaviour
{
    public TextMeshProUGUI leftChickenText;
    public GameObject nextButton;

    public void Activate()
    {
        int points;
        Time.timeScale = 1f;
        gameObject.SetActive(true);
        nextButton.SetActive(true);
        FoxScript fox = FindObjectOfType<FoxScript>();
        points = (3 - fox.ChickenLeftOnMap);
        leftChickenText.text = "Congratulations Fox ate " + points + " chickens on the way to hen";
        Debug.Log("lvl " + CurrentLevel.LevelNumber + " "+PlayerPrefs.GetInt("Level_" + CurrentLevel.LevelNumber));
        if (points > PlayerPrefs.GetInt("Level_" + CurrentLevel.LevelNumber))
        {
            PlayerPrefs.SetInt("Level_" + CurrentLevel.LevelNumber, points);
        }
        PlayerPrefs.SetInt("LastLevel",CurrentLevel.LevelNumber+1);
    }

    public void ActivateKill()
    {
        Time.timeScale = 1f;
        nextButton.SetActive(false);
        gameObject.SetActive(true);
        leftChickenText.text = "Fox has been injured";
    }

    public void GotoNextLevelButtonFunction()
    {
        LevelManager.Instance.GotoNextLevel();
    }
    public void RestartLevelButtonFunction()
    {
        LevelManager.Instance.RestartLevel();
    }
}