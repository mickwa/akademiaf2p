﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public enum CardTypes
{
  NONE,
  DIRECTION_UP,
  DIRECTION_DOWN,
  DIRECTION_LEFT,
  DIRECTION_RIGHT,
  BONE,
  PATCH,
  SEEDS
}

[RequireComponent(typeof(CanvasGroup))]
[RequireComponent(typeof(Image))]
public abstract class Card : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
{
  public const string CardTag = "Field";
  private CardManager _cardManager;
  public CardTypes cardType;
  public RectTransform rectTransform;
  public Image image;
  public int cost;
  private FoxScript Fox;
  bool canBeDragged;
    bool corectlyDragStart=false;

  void Start()
  {
    _cardManager = CardManager.Instance;
    rectTransform = GetComponent<RectTransform>();
    image = GetComponent<Image>();
  }

  private void RegisterEvents()
  {
    Fox.OnFoxPowerChanged += Fox_OnFoxPowerChanged;
  }

  private void UnregisterEvents()
  {
    if (Fox != null)
    {
      Fox.OnFoxPowerChanged -= Fox_OnFoxPowerChanged;
    }
  }

  private void Fox_OnFoxPowerChanged()
  {
    UpdateCardView();
  }

  private void UpdateCardView()
  {
    if (Fox == null)
    {
      Fox = FindObjectOfType<FoxScript>();
    }
    if (Fox != null)
    {
      canBeDragged = (Fox.CurrentPower >= cost);
      image.color = canBeDragged ? Color.white : Color.white * 0.7f;
    }
  }

  void Update()
  {
    if (Fox == null)
    {
      Fox = FindObjectOfType<FoxScript>();
      if (Fox != null)
      {
        Fox_OnFoxPowerChanged();
        Debug.Log("Found fox for card: " + name);
        RegisterEvents();
      }
    }
  }

  void OnDestroy()
  {
    UnregisterEvents();
  }

  public void OnDrag(PointerEventData eventData)
  {
    if (((LevelManager.Instance.CurrentGameState == GameState.GAMEPLAY) ||
        (LevelManager.Instance.CurrentGameState == GameState.TUTORIAL)) && 
        canBeDragged && 
        corectlyDragStart)
    {

      transform.Translate(eventData.delta);
    }
  }

  public abstract void CardAction();

  public void OnBeginDrag(PointerEventData eventData)
  {
    if (((LevelManager.Instance.CurrentGameState == GameState.GAMEPLAY) || 
         (LevelManager.Instance.CurrentGameState == GameState.TUTORIAL)) &&
         canBeDragged)
    {
      corectlyDragStart = true;
      _cardManager.SetSelectedCard(this);
    }
  }

  public void OnEndDrag(PointerEventData eventData)
  {
    if ((LevelManager.Instance.CurrentGameState == GameState.GAMEPLAY)||(LevelManager.Instance.CurrentGameState == GameState.TUTORIAL))
    {
      corectlyDragStart = false;
      _cardManager.ReleaseCard();
      UpdateCardView();
    }
  }

  public void MarkCardAsCanBeUsed(bool v)
  {
    image.color = v ? Color.green : Color.red;
  }
}