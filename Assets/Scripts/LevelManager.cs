﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public enum GameState
{
  COUNTDOWN,
  GAMEPLAY,
  FINISH,
  TUTORIAL
}

public class LevelManager : MonoBehaviour
{
    public float TimeToWait;
    [HideInInspector]
    public float TimeLeft;
    public GameState CurrentGameState;
    public static LevelManager Instance = null;
    public event Action OnTimeLeftChanged;
    public event Action OnGameStart;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        if (Instance != this)
            Destroy(gameObject);
    }

    void Start()
    {
        TimeLeft = TimeToWait;
        if((PlayerPrefs.GetInt("EasyMode")!=0)||(PlayerPrefs.GetInt("TutorialPassed")!=1))
        {
            Time.timeScale = 0.5f;
        }
        else
        {
            Time.timeScale = 1f;
        }
    }
   
    void Update()
    {
        // countdown:
        if (CurrentGameState == GameState.COUNTDOWN)
        {
            TimeLeft -= Time.deltaTime;
            if (TimeLeft >= 0)
            {
                if (OnTimeLeftChanged != null)
                {
                    OnTimeLeftChanged();
                }
            }
            else
            {
                if (PlayerPrefs.GetInt("TutorialPassed") != 1 && CurrentLevel.LevelNumber==1)
                {
                    CurrentGameState = GameState.TUTORIAL;
                }
                else
                {
                    CurrentGameState = GameState.GAMEPLAY;
                }
                if (OnGameStart != null)
                {
                    OnGameStart();
                }
            }
        }
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene("mainMenu");
        }
    }

    public void GotoNextLevel()
    {
        //PlayerPrefs.SetInt("Level_" + CurrentLevel.LevelNumber.ToString(), 1);
        /*if (CurrentLevel.LevelNumber > PlayerPrefs.GetInt("LastLevel"))
        {
            PlayerPrefs.SetInt("LastLevel", CurrentLevel.LevelNumber);
        }*/
        //CurrentLevel.LevelNumber++;
        if (CurrentLevel.LevelNumber <= CurrentLevel.TotalLevels)
        {
            SceneManager.LoadScene("Loader");
        }
        else
        {
            SceneManager.LoadScene("Credits",LoadSceneMode.Single);
        }
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene("Loader");
    }
    public void Lost()
    {
        UIController.Instance.ShowLostLevelPanel();
    }
    public void ShowEndLevelUI()
    {
        UIController.Instance.ShowFinishedLevelPanel();
    }
}