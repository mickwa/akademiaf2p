﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

    public void startGame()
    {
        SceneManager.LoadScene("Levels", LoadSceneMode.Single);
    }
    public void options()
    {
        SceneManager.LoadScene("Options", LoadSceneMode.Single);
    }
    public void Instructions()
    {
        SceneManager.LoadScene("Instructions", LoadSceneMode.Single);
    }
    public void credits()
    {
        SceneManager.LoadScene("Credits", LoadSceneMode.Single);
    }
    public void quit()
    {
        Application.Quit();
    }
}
