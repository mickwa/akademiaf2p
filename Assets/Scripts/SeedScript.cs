﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeedScript : MonoBehaviour {

    public float publicLifetime;
    public float publicActiveRadius;

    Collider2D[] _hens;
    int _privateFindHens;
    float _privteMinDistance;
    Transform _privateClosestHen;
    

    // Use this for initialization
    void Start()
    {
        _privteMinDistance = Mathf.Infinity;
        _privateClosestHen = null;
        StartCoroutine(Activate());
    }

    IEnumerator Activate()
    {
        bool henfound = false;
        yield return new WaitForSeconds(0.3f);
        _hens = Physics2D.OverlapCircleAll(transform.position, publicActiveRadius);
        // this is for chicken
        for (int i = 0; i < _hens.Length; ++i)
        {
            if (_hens[i].CompareTag("Target"))
            {
                _hens[i].GetComponent<HenScript>().enabled = false;
                _hens[i].GetComponent<MoveToSeedScript>().enabled = true;
                _hens[i].GetComponent<MoveToSeedScript>().publicDestination = transform.position;
                henfound = true;
                break;
            }
        }
        
        if (!henfound)
        {
           // Debug.Log("hens not found" + _hens.Length);
            for (int i = 0; i < _hens.Length; ++i)
            {
                //Debug.Log(_hens[i].tag);
                if (_hens[i].CompareTag("Chicken"))
                {

                    float distance = (transform.position - _hens[i].transform.position).magnitude;
                    if(distance<_privteMinDistance)
                    {
                        _privateClosestHen = _hens[i].transform;
                    }
                    //Debug.Log("chicken");
                }
            }
            if(_privateClosestHen!=null)
            {
                _privateClosestHen.GetComponent<HenScript>().enabled = false;
                _privateClosestHen.GetComponent<MoveToSeedScript>().enabled = true;
                _privateClosestHen.GetComponent<MoveToSeedScript>().publicDestination = transform.position;
            }
        }

        yield return new WaitForSeconds(publicLifetime);

        henfound=false;

    foreach (BoxCollider2D c in _hens)
    {
      if (c != null && c.GetComponent<HenScript>() != null && c.GetComponent<MoveToSeedScript>() != null) // to są kurczaki 
      {
        c.GetComponent<HenScript>().enabled = true;
        c.GetComponent<MoveToSeedScript>().enabled = false;
      }
    }
    //for (int i = 0; i < _hens.Length; ++i)
    //{
    //    if (_hens[i].CompareTag("Target"))
    //    {
    //        _hens[i].GetComponent<HenScript>().enabled = true;
    //        _hens[i].GetComponent<MoveToSeedScript>().enabled = false;
    //        henfound= true;
    //        break;
    //    }
    //}
    //if (!henfound)
    //{
    //    for (int i = 0; i < _hens.Length; ++i)
    //    {
    //        if (_hens[i].CompareTag("Chicken"))
    //        {
    //            _hens[i].GetComponent<HenScript>().enabled = true;
    //            _hens[i].GetComponent<MoveToSeedScript>().enabled = false;
    //            break;
    //        }
    //    }
    //}
    transform.parent.GetComponent<Field>().IsSomethingThere = false;
        Destroy(gameObject);
    }
}
