﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class CardManager : MonoBehaviour
{
  public static CardManager Instance = null;
  private Field currentField;
  private bool canUseCard;

  public Card SelectedCard
  {
    get;
    private set;
  }
    private void Start()
    {
        Debug.Log("Start");
    }

    public void Awake()
  {
        Debug.Log("Awake");
    if (Instance == null)
      Instance = this;
    if(Instance!=this)
        Destroy(gameObject);
  }

  void Update()
  {
    if (SelectedCard == null) return;

    canUseCard = CanSelectedCardBeUsed();
    SelectedCard.MarkCardAsCanBeUsed(canUseCard);
  }

  public void SetCurrentField(Field f)
  {
    currentField = f;
    //Debug.Log("Current selected field: " + (f == null ? "none" : f.name));
   // Debug.Log("Current selected card: " + (SelectedCard == null ? "none" : SelectedCard.name));
  }

  public void SetSelectedCard(Card selectedCard)
  {
    Debug.Log("Selected Card of type: " + selectedCard.GetComponent<Card>().cardType);
    SelectedCard = selectedCard;
    SelectedCard.transform.localScale = Vector2.one * 0.5f;
    SelectedCard.GetComponent<CanvasGroup>().alpha = 0.3f;
  }

  public void ReleaseCard()
  {
    if (SelectedCard == null) return;
    if (canUseCard)
    {
      //Debug.Log("Use Card "+currentField.name);
      currentField.ApplyCard(SelectedCard.cardType);
      FindObjectOfType<FoxScript>().CurrentPower -= SelectedCard.cost;
    }
    ClearSelectedCard();
  }

  public void ClearSelectedCard()
  {
    if (SelectedCard == null) return; // this should never happen, but happens
    SelectedCard.rectTransform.anchoredPosition = Vector2.zero;
    SelectedCard.transform.localScale = Vector2.one;
    SelectedCard.GetComponent<CanvasGroup>().alpha = 1f;
    SelectedCard.image.color = Color.white;
    SelectedCard = null;
  }

  public bool CanSelectedCardBeUsed()
  {
    if (SelectedCard == null) return false;

    // if mouse is not over any field, card can not be used for sure.
    if (currentField == null)
    {
      Debug.Log("Can not put anything: current field is null");
      return false;
    }
    
    if (currentField.name == "Tree")
    {
      Debug.Log("Can not put anything: current field is tree");
      return false;
    }

    // If on current field some item is already, you can not put anything else
    if (currentField.IsSomethingThere == true)
    {
      Debug.Log("current field " + currentField.name + " has something on it");
      return false;
    }
    // here we have both field and card:

    if (SelectedCard != null && currentField != null)
    {
      if (SelectedCard.cardType == CardTypes.PATCH)
      {
        return currentField.name.Contains("Hole");  // Patches can be used only over holes.
      }

      if (SelectedCard.cardType == CardTypes.DIRECTION_UP ||
        SelectedCard.cardType == CardTypes.DIRECTION_DOWN ||
        SelectedCard.cardType == CardTypes.DIRECTION_LEFT ||
        SelectedCard.cardType == CardTypes.DIRECTION_RIGHT)
      {
        return (currentField.name.Contains("Ground") || currentField.name.Contains("Leafs")); // Direction card, can be put only on grass.
            }

      if (SelectedCard.cardType == CardTypes.BONE)
      {
        return (currentField.name.Contains("Ground") || currentField.name.Contains("Ice") || currentField.name.Contains("Leafs") || currentField.name.Contains("BearTrap") ); // Bone card, can be put only on grass
            }

      if (SelectedCard.cardType == CardTypes.SEEDS)
      {
        return (currentField.name.Contains("Ground") || currentField.name.Contains("Ice") || currentField.name.Contains("Leafs")); // seeds card, can be put only on grass
            }
    }
    return false;
  }
}