﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeafPileScript : Field
{

    public GameObject WhatIsUnder;
    public GameObject Tornado;
    private SpriteRenderer _privateSpriteRenderer;
    public Sprite LeafTile;
    public Sprite GreenTile;

    private void Start()
    {
        _privateSpriteRenderer = GetComponent<SpriteRenderer>();
        _privateSpriteRenderer.color = new Color(1f, 1f, 1f, 1f);
        _privateSpriteRenderer.sprite = LeafTile;
        Tornado.SetActive(false);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            _privateSpriteRenderer.enabled = false;
            Tornado.SetActive(true);
            GameObject temp = Instantiate(WhatIsUnder, transform);
            temp.transform.parent = null;
            Destroy(gameObject, 3f);
        }
    }

    public override void ApplyCard(CardTypes type)
    {
        if (LevelManager.Instance.CurrentGameState == GameState.TUTORIAL)
        {
            LevelManager.Instance.CurrentGameState = GameState.GAMEPLAY;
        }
        GameObject item = ItemsLoader.LoadResource(type);
        GameObject temp = Instantiate(item, transform) as GameObject;
        IsSomethingThere = true;
        temp.transform.parent = transform;
    }

}
