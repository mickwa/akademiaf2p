﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MapElements
{
  NONE,
  GROUND
}

public static class MapElementsSpawner
{
  public static GameObject GetMapElement(MapElements mapElement)
  {
    GameObject item = null;

    switch (mapElement)
    {
      case (MapElements.GROUND):
        {
          item = Resources.Load<GameObject>("MapElements/Ground");
          break;
        }
    }

    if (item == null)
      Debug.Log("Unsupported map element: " + mapElement);
    return item;
  }
}