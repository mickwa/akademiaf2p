﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoneScript : MonoBehaviour {

    public float publicLifetime;
    public float publicActiveRadius;

    Collider2D[] _privateHounds;
    int _privateFindHounds;

    // Use this for initialization
    void Start () {
        StartCoroutine(Activate());
	}
	
    IEnumerator Activate()
    {
        yield return new WaitForSeconds(0.3f);
//        _privateHounds = new Collider2D[];
        _privateHounds = Physics2D.OverlapCircleAll(transform.position, publicActiveRadius);

        //Debug.Log(" znalazlo " + _privateFindHounds);

        for (int i = 0; i < _privateHounds.Length; ++i)
        {
            if (_privateHounds[i].CompareTag("Enemy"))
            {
                _privateHounds[i].GetComponent<HoundScript>().enabled = false;
                _privateHounds[i].GetComponent<MoveToBoneScript>().enabled = true;
                _privateHounds[i].GetComponent<MoveToBoneScript>().publicDestination = transform.position;
            }
        }

        yield return new WaitForSeconds(publicLifetime);

        for (int i = 0; i < _privateHounds.Length; ++i)
        {
            if (_privateHounds[i].CompareTag("Enemy"))
            {
                _privateHounds[i].GetComponent<HoundScript>().enabled = true;
                _privateHounds[i].GetComponent<MoveToBoneScript>().enabled = false;
            }
        }
        transform.parent.GetComponent<Field>().IsSomethingThere = false;
        Destroy(gameObject);
    }
}
