﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Start game with this scene.
/// Used to load all scenes, to avoid conflicts.
/// </summary>
public class GameLoader : MonoBehaviour
{ 
  void Start()
  {
    SceneManager.LoadScene("UIScene",LoadSceneMode.Additive);
    SceneManager.LoadScene("Level "+CurrentLevel.LevelNumber, LoadSceneMode.Additive);
  }
}