﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
  CardManager cardManager;
  InputHandler Instance = null;

  void Awake()
  {
    if (Instance == null)
      Instance = this;
    if (Instance != this)
        Destroy(gameObject);
  }

  void Start()
  {
    cardManager = CardManager.Instance;
    cardManager.SetCurrentField(null);
   }

  void Update()
  {
    Vector2 mousePos = Input.mousePosition;
    Vector2 raycastStart = Camera.main.ScreenToWorldPoint(mousePos);

    // There can be more than one collider if on field there is item
    RaycastHit2D[] hit = Physics2D.RaycastAll(raycastStart, Vector3.forward);


    if (hit.Length == 0) // if totally nothing was hit:
    {
      cardManager.SetCurrentField(null);
    }
    else
    {
      for (int i = 0; i < hit.Length; i++) // check if any collider is a map field
      {
        Field hitField = hit[i].collider.GetComponent<Field>();
        if (hitField != null)
        {
                    Debug.Log("field " + hitField.name);
          // Update over which map field cursor currently is:
          cardManager.SetCurrentField(hitField);
          // (cards are set in cardManager in Card class in drag handlers)
        }
      }
    }
  }
}