﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OptionMenuController : MonoBehaviour {

    public Toggle easyMode;
    public Toggle tutorialMode;
    public GameObject popup;

	// Use this for initialization
	void Start () {
        if(easyMode!=null)
            easyMode.isOn = PlayerPrefs.GetInt("EasyMode") == 1;
        if(popup!=null)
            popup.SetActive(false);
        if (tutorialMode != null)
            tutorialMode.isOn = PlayerPrefs.GetInt("TutorialPassed") == 0;
    }
	public void toggleMode()
    {
        PlayerPrefs.SetInt("EasyMode", easyMode.isOn?1:0);
    }
    public void toggleTutorial()
    {
        PlayerPrefs.SetInt("TutorialPassed", tutorialMode.isOn?0:1);
    }
    public void Back()
    {
        SceneManager.LoadScene("mainMenu", LoadSceneMode.Single);
    }
    public void EraseAll()
    {
        popup.SetActive(true);
    }
    public void Approved()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.SetInt("EasyMode", easyMode.isOn ? 1 : 0);
        PlayerPrefs.SetInt("LastLevel", 1);
        PlayerPrefs.SetInt("TutorialPassed", 0);
        tutorialMode.isOn = PlayerPrefs.GetInt("TutorialPassed") == 0;
        popup.SetActive(false);
    }
    public void Cancel()
    {
        popup.SetActive(false);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene("mainMenu");
        }
    }
}
