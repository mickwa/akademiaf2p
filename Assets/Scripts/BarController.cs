﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarController : MonoBehaviour
{
  public RectTransform BarFill;
  private RectTransform Background;
  FoxScript Fox;

  private float barWidth;

  void Start()
  {
    Background = GetComponent<RectTransform>();
    barWidth = Background.rect.width;
  }

  /// <summary>
  /// Value is from 0 to 1
  /// </summary>
  /// <param name="value"></param>
  public void SetBarValue(float value)
  {
    BarFill.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Mathf.Lerp(0, barWidth,value));
  }

  public void Update()
  {
    if(Fox == null)
      Fox = FindObjectOfType<FoxScript>();  
    // to do: make this by event listener not in Update
    SetBarValue(Fox.GetCurrentPower01());
  }
}