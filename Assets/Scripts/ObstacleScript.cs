﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleScript : MonoBehaviour
{

  private void OnTriggerEnter2D(Collider2D collision)
  {
    if (collision.CompareTag("Player"))
    {
        Destroy(collision.gameObject,1f);
        collision.GetComponent<SpriteRenderer>().enabled = false;
        collision.GetComponent<ParticleSystem>().Play();
        Debug.Log("die!");
        LevelManager.Instance.Lost();
    }
  }
}
