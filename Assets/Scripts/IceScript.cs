﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceScript : Field {

    // Use this for initialization
    void Awake () {
        //IsSomethingThere = true;
    }

    public override void ApplyCard(CardTypes type)
    {
        switch (type)
        {
            case CardTypes.BONE: 
            case CardTypes.PATCH:
            case CardTypes.SEEDS:
                GameObject item = ItemsLoader.LoadResource(type);
                Instantiate(item, transform);
                IsSomethingThere = true;
            break;
            default:
            break;
        }
    }
}
