﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class Field : MonoBehaviour
{
  public abstract void ApplyCard(CardTypes type);

  public bool IsSomethingThere = false;
}
