﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundScript : Field
{
  public GameObject PublicObjectToSpawn;
  public GameObject PublicSpawnedObject;

  private SpriteRenderer _privateSpriteRenderer;
  public Sprite GreenTile;
  public bool publicCardsApllied;
  
  void Start()
  {
        // Sprite renderer is used only in editor to see where tiles of grass are placed.
        //Destroy(GetComponent<SpriteRenderer>());
        _privateSpriteRenderer = GetComponent<SpriteRenderer>();
        _privateSpriteRenderer.color = new Color(1f, 1f, 1f, 0f);
        _privateSpriteRenderer.sprite = GreenTile;
    }

    public override void ApplyCard(CardTypes type)
    {
        
        GameObject item = ItemsLoader.LoadResource(type);
        Instantiate(item, transform);
        IsSomethingThere = true;
        //item.transform.parent = transform;
    }

    private void OnMouseOver()
    {
        _privateSpriteRenderer.color = new Color(1f, 1f, 1f, 1f);
    }

    private void OnMouseExit()
    {
        _privateSpriteRenderer.color = new Color(1f, 1f, 1f, 0f);
    }   
}