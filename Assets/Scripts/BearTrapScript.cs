﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BearTrapScript : MonoBehaviour {
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Destroy(collision.gameObject,1f);
            collision.GetComponent<SpriteRenderer>().enabled = false;
            collision.GetComponent<ParticleSystem>().Play();
            LevelManager.Instance.Lost();
        }
        else if(collision.CompareTag("Hound")||collision.CompareTag("Chicken"))
        {
            Destroy(collision.gameObject);
        }
        Destroy(gameObject);
    }
}
