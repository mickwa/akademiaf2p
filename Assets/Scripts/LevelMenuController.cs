﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelMenuController : MonoBehaviour {

    public Transform[] levelButtons;
    public GameObject nextButton;
    public string txt;
    public Scene[] scenes;
    int PlayableLevels;
    int page = 0;
    int ButtonsAmount = 9;
    int maxPages;
    int currentPage;
    void Awake () {
        levelButtons = new Transform[9];
        ButtonsAmount = transform.childCount;

        //PlayerPrefs.SetInt("LastLevel", 1);
        //CurrentLevel.TotalLevels = 5;
        // Debug.Log(PlayerPrefs.GetInt("Level_1"));
        // Debug.Log(PlayerPrefs.GetInt("Level_2"));
        // Debug.Log(PlayerPrefs.GetInt("Level_3"));
        if (PlayerPrefs.GetInt("LastLevel") < 1)
        {
            PlayerPrefs.SetInt("LastLevel", 1);
        }
        PlayableLevels = PlayerPrefs.GetInt("Level_"+PlayerPrefs.GetInt("LastLevel").ToString())>0?PlayerPrefs.GetInt("LastLevel"): PlayerPrefs.GetInt("LastLevel")-1;
        if (PlayableLevels < 1)
            PlayableLevels = 1;
        maxPages = Mathf.CeilToInt(CurrentLevel.TotalLevels / 9.0f)-1;
        page = 0;
        for (int i = 0; i < ButtonsAmount; ++i)
        {
            levelButtons[i] = transform.GetChild(i);
        }
        DisplayPage();
    }
    public void DisplayPage()
    {
        if (page < maxPages)
        {
            nextButton.SetActive(true);
        }
        else
        {
            nextButton.SetActive(false);
        }

        for (int i = 0; i < ButtonsAmount; ++i)
        {
            SetLevelState(i, (page*9)+i + 1, (page * 9)+i <= PlayableLevels, PlayerPrefs.GetInt("Level_" + ((page * 9)+(i+1)).ToString()));
        }
    }
    public void SetLevelState(int level,int value ,bool state,int eggs=0)
    {
        if (value <= CurrentLevel.TotalLevels)
        {
            levelButtons[level].GetComponent<Image>().enabled = true;
            levelButtons[level].Find("Text").GetComponent<Text>().enabled = true;
            SetEggs(level, eggs);
            if (state)
            {
                levelButtons[level].Find("Hen").gameObject.SetActive(false);
                prepareButton(level, value);
                //SetEggs(level, eggs);
            }
            else
            {
                levelButtons[level].Find("Hen").gameObject.SetActive(true);
                clearButton(level,value);
                //SetEggs(level, 0);
            }
        }
        else
        {
            levelButtons[level].GetComponent<Image>().enabled = false;
            levelButtons[level].Find("Text").GetComponent<Text>().enabled = false;
            levelButtons[level].Find("Hen").gameObject.SetActive(false);
            //prepareButton(level, value);
            SetEggs(level, 0);
        }
    }
    public void clearButton(int level,int value)
    {
        levelButtons[level].GetComponent<Button>().onClick.RemoveAllListeners();
        levelButtons[level].Find("Text").GetComponent<Text>().text = value.ToString();
    }
    public void prepareButton(int level,int value)
    {
        levelButtons[level].GetComponent<Button>().onClick.AddListener(delegate { LoadLevel(value); });
        levelButtons[level].Find("Text").GetComponent<Text>().text = value.ToString();
    }

    public void SetEggs(int level,int eggs)
    {
        int ile = levelButtons[level].Find("Eggs").childCount;
        for(int i=0;i<ile;++i)
        {
            levelButtons[level].Find("Eggs").GetChild(i).gameObject.SetActive(i < eggs);
        }
    }

    public void LoadLevel(int level)
    {
        Debug.Log("loading level " + level.ToString());

        CurrentLevel.LevelNumber = level;
        SceneManager.LoadScene("Loader", LoadSceneMode.Single);
    }


    public void loadNext()
    {
        page++;
        Debug.Log(page + " " + maxPages);
        if (page <= maxPages)
        {
            DisplayPage();
        }
        else
        {
            nextButton.SetActive(false);
            page--;
        }
    }
    public void loadPrevious()
    {
        if (page > 0)
        {
            page--;
            DisplayPage();
        }
        else
        {
            SceneManager.LoadScene("mainMenu");
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene("mainMenu");
        }
    }
}
