﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartScript : MonoBehaviour
{ 
  public GameObject PublicFoxModel;
  public FoxDirection publicDirection = FoxDirection.top;
   
  void Start()
  {
    GameObject temp = Instantiate(PublicFoxModel, transform);
    temp.transform.parent = null;
    temp.GetComponent<FoxScript>().publicDirection = publicDirection;
    temp.GetComponent<FoxScript>().changeDirection(publicDirection);
    temp.GetComponent<FoxScript>().UpdateDirection();
    Destroy(GetComponent<SpriteRenderer>());
  }
}