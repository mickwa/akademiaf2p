﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoleScript : Field
{
  public override void ApplyCard(CardTypes type)
  {
    GameObject groundPrefab = MapElementsSpawner.GetMapElement(MapElements.GROUND);
    Instantiate(groundPrefab, transform.position, Quaternion.identity);
    Destroy(gameObject);
  }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
          Destroy(collision.gameObject,1f);
          LevelManager.Instance.Lost();
        }
    }
}