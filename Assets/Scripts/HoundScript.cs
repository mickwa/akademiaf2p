﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoundScript : MonoBehaviour {

    public Transform[] publicMovementPoints;
    public float publicMovementSpeed;
    public float publicStopDistance;
    public bool a2b = true;

    private int _privateSelectedPoint=0;
    private MoveToBoneScript _privateMoveToBone;

    private Animator _privateHoundAnim;
    private SpriteRenderer sprenderer;


    FoxDirection houndDirectionOld;
    FoxDirection houndDirectionNew;
    // Use this for initialization
    void Start () {
        _privateMoveToBone = GetComponent<MoveToBoneScript>();
        _privateMoveToBone.enabled = false;
        _privateHoundAnim = GetComponent<Animator>();
        sprenderer = GetComponent<SpriteRenderer>();
        houndDirectionOld = FoxDirection.top;
        _privateHoundAnim.SetTrigger("Top");
    }
	
	// Update is called once per frame
	void Update () {

        if (publicMovementPoints.Length > 0)
        {
            if (a2b)
            {
                //Vector3 movement = new Vector3();

                Vector3 distance = publicMovementPoints[_privateSelectedPoint].position - transform.position;

                Animate(distance);

                transform.position = Vector3.MoveTowards(transform.position, publicMovementPoints[_privateSelectedPoint].position, publicMovementSpeed * Time.deltaTime);

                distance = new Vector3(distance.x, distance.y, 0f);

                if (distance.magnitude < publicStopDistance)
                {
                    _privateSelectedPoint++;
                    _privateSelectedPoint %= publicMovementPoints.Length;
                }
            }
            else
            {
                Vector3 distance = publicMovementPoints[_privateSelectedPoint].position - transform.position;

                Animate(distance);
                transform.position = Vector3.MoveTowards(transform.position, publicMovementPoints[_privateSelectedPoint].position, publicMovementSpeed * Time.deltaTime);

                distance = new Vector3(distance.x, distance.y, 0f);

                if (distance.magnitude < publicStopDistance)
                {
                    _privateSelectedPoint++;
                    _privateSelectedPoint %= publicMovementPoints.Length; a2b = true;
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            collision.GetComponent<SpriteRenderer>().enabled = false;
            collision.GetComponent<ParticleSystem>().Play();
            Debug.Log("die!");
            LevelManager.Instance.Lost();
        }
    }

    private void Animate(Vector3 distance)
    {
        if (distance.x >= 0 && distance.y >= 0)
        {
            if (distance.x >= distance.y)
            {
                houndDirectionNew = FoxDirection.left;
                //_privateHoundAnim.SetTrigger("Left");
            }
            else
            {
                houndDirectionNew = FoxDirection.top;
                //_privateHoundAnim.SetTrigger("Top");
            }
        }
        else if (distance.x >= 0 && distance.y <= 0)
        {
            if (Mathf.Abs(distance.x) >= Mathf.Abs(distance.y))
            {
                houndDirectionNew = FoxDirection.left;
                //_privateHoundAnim.SetTrigger("Left");
            }
            else
            {
                houndDirectionNew = FoxDirection.bottom;
                //_privateHoundAnim.SetTrigger("Bottom");
            }
        }
        else if (distance.x <= 0 && distance.y >= 0)
        {
            if (Mathf.Abs(distance.x) >= Mathf.Abs(distance.y))
            {
                houndDirectionNew = FoxDirection.right;
                //_privateHoundAnim.SetTrigger("Right");
            }
            else
            {
                houndDirectionNew = FoxDirection.top;
                //_privateHoundAnim.SetTrigger("Top");
            }
        }
        else
        {
            if (distance.x <= 0 && distance.y <= 0)
            {
                if (Mathf.Abs(distance.x) >= Mathf.Abs(distance.y))
                {
                    houndDirectionNew = FoxDirection.right;
                    //_privateHoundAnim.SetTrigger("Right");
                }
                else
                {
                    houndDirectionNew = FoxDirection.bottom;
                    //_privateHoundAnim.SetTrigger("Bottom");
                }
            }
        }
        if (houndDirectionOld != houndDirectionNew)
        {
            houndDirectionOld = houndDirectionNew;
            switch (houndDirectionOld)
            {
                case FoxDirection.bottom:
                    Debug.Log("Bottom");
                    _privateHoundAnim.SetTrigger("Bottom");
                    sprenderer.flipX = false;
                    break;
                case FoxDirection.top:
                    Debug.Log("top");
                    _privateHoundAnim.SetTrigger("Top");
                    sprenderer.flipX = false;
                    break;
                case FoxDirection.left:
                    Debug.Log("right");
                    _privateHoundAnim.SetTrigger("Right");
                    sprenderer.flipX = true;
                    break;
                case FoxDirection.right:
                    Debug.Log("left");
                    _privateHoundAnim.SetTrigger("Left");
                    sprenderer.flipX = false;
                    break;
            }
        }
    }
}
