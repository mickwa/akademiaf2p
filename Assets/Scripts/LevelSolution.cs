﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="LevelSolution")]
public class LevelSolution : ScriptableObject
{
 public CardTypes[] CardsOrder;
}