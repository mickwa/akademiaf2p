﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using TMPro;

public class StartGameCountDown : MonoBehaviour
{
  public TextMeshProUGUI CounterText;
  public TextMeshProUGUI CurrentLevelText;

  LevelManager levelManager;

  public Animator PublicAnimator;
  public GameObject TutorialFinger;


  void Awake()
  {
    TutorialFinger.SetActive(false);
    PublicAnimator.speed = 0f;
    levelManager = LevelManager.Instance;
    if(PlayerPrefs.GetInt("TutorialPassed") != 1 && CurrentLevel.LevelNumber==1) 
    {
        CurrentLevelText.text = "Starting\nTutorial";
    }
    else 
    {
        CurrentLevelText.text = " Starting \n Level " + CurrentLevel.LevelNumber;
    }
    RegisterEvents();
  }

  void Destroy()
  {
    UnregisterEvents();
  }

  void RegisterEvents()
  {
    levelManager.OnTimeLeftChanged += LevelManager_OnTimeLeftChanged;
    levelManager.OnGameStart += LevelManager_OnGameStart;
  }

  void UnregisterEvents()
  {
    levelManager.OnTimeLeftChanged -= LevelManager_OnTimeLeftChanged;
    levelManager.OnGameStart -= LevelManager_OnGameStart;
  }

  private void LevelManager_OnGameStart()
  {
    if (PlayerPrefs.GetInt("TutorialPassed") != 1 && CurrentLevel.LevelNumber == 1)
    {
            CounterText.enabled = false;
            CurrentLevelText.text = "Drag Card";
            PublicAnimator.speed = 1;
            TutorialFinger.SetActive(true);
            //PlayerPrefs.SetInt("TutorialPassed",1);
    }
    else
    {
            CurrentLevelText.text = " Starting \n Level " + CurrentLevel.LevelNumber;
            PublicAnimator.speed = 0f;            
            Destroy(gameObject);
    }
  }
  public void TutorialShow()
  {
    LevelManager_OnGameStart();
  }

  public void ShowTapInfo()
  {
    CurrentLevelText.text = "Tap Arrow";
  }
  private void LevelManager_OnTimeLeftChanged()
  {
    UpdateCounterText();
  }
  
  private void UpdateCounterText()
  {
    CounterText.text = ((int)levelManager.TimeLeft).ToString();
  }
}