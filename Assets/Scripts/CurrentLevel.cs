﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CurrentLevel
{
  public static int LevelNumber = 1;
  public static int TotalLevels = 20;
}