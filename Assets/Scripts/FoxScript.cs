﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum FoxDirection
{
    top,
    left,
    right,
    bottom
}


public class FoxScript : MonoBehaviour {
  [Header("Points for items:")]
  public int PointsForChicken;

    public FoxDirection publicDirection;
    private FoxDirection _privateNewFoxDirection;
    public float publicSpeed;
    public float publicTurningSpeed;
    private bool _privateTurning;
    private bool _privateMoveToCenter;
    private Vector3 _privateCenterPosition;
    public float StartPower;
    private float _currentPower;
    public float CurrentPower
    {
      get
      {
        return _currentPower;
      }

      set
      {
        if (_currentPower != value)
        {
          _currentPower = value;
          if (OnFoxPowerChanged != null) OnFoxPowerChanged();
        }
      }
    }
    Collider2D _privateStrzalka;
    private Animator _privateAnimator;
    private SpriteRenderer _privateSpriteRenderer;

    int eatenChickens = 0;
    public int ChickenLeftOnMap
    {
        get;
        private set;
    }

    public event Action  OnFoxPowerChanged;

    private void Awake()
    {
        _privateAnimator = GetComponent<Animator>();
        _privateSpriteRenderer = GetComponent<SpriteRenderer>();
        CurrentPower = StartPower;
    }

    void Update () {
    if (LevelManager.Instance.CurrentGameState != GameState.GAMEPLAY) return;
        if (_privateTurning)
        {
            if (_privateMoveToCenter)
            {
                Vector3 distance = transform.position - _privateCenterPosition;
                distance = new Vector3(distance.x, distance.y, 0f);
                //Debug.Log("d " + distance.magnitude);
                if (distance.magnitude > 0.1f)
                {
                    moveFox();
                }
                else
                {
                    _privateMoveToCenter = false;
                    transform.position = _privateCenterPosition;
                    publicDirection = _privateStrzalka.GetComponent<TurnScript>().publicDirection;
                }
            }
            else
            {
                switch (publicDirection)
                {
                    case FoxDirection.top:
                        /*transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0f, 0f, 0f)), publicTurningSpeed);
                        if (transform.rotation.ToEuler().z < 5f && transform.rotation.ToEuler().z > -5f)
                        {*/
                            //transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
                            _privateTurning = false;
                            _privateAnimator.SetTrigger("Top");
                            _privateSpriteRenderer.flipX = false;
                        //}
                        break;
                    case FoxDirection.bottom:
                        /*transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0f, 0f, 180f)), publicTurningSpeed);
                        if (transform.rotation.ToEuler().z < 185f && transform.rotation.ToEuler().z > 175f)
                        {*/
                            //transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 180f));
                            _privateTurning = false;
                            _privateAnimator.SetTrigger("Bottom");
                            _privateSpriteRenderer.flipX = false;
                        //}

                        break;
                    case FoxDirection.left:
                        /*transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0f, 0f, 90f)), publicTurningSpeed);
                        if (transform.rotation.ToEuler().z < 95f && transform.rotation.ToEuler().z > 85f)
                        {*/
                            //transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 90f));
                            _privateTurning = false;
                            _privateAnimator.SetTrigger("Left");
                            _privateSpriteRenderer.flipX = false;
                        //}
                        break;
                    case FoxDirection.right:
                        /*transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0f, 0f, 270f)), publicTurningSpeed);
                        if (transform.rotation.ToEuler().z < 275f && transform.rotation.ToEuler().z > 265f)
                        {*/
                            //transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 270f));
                            _privateTurning = false;
                            _privateAnimator.SetTrigger("Right");
                            _privateSpriteRenderer.flipX = true;
                        //}
                        break;
                }
            }
        }
        else
        {
            moveFox();
        }
	}

    private void moveFox()
    {
        switch (publicDirection)
        {
            case FoxDirection.top:
                transform.position = new Vector3(transform.position.x, transform.position.y + publicSpeed * Time.deltaTime, transform.position.z);
                break;
            case FoxDirection.bottom:
                transform.position = new Vector3(transform.position.x, transform.position.y - publicSpeed * Time.deltaTime, transform.position.z);
                break;
            case FoxDirection.left:
                transform.position = new Vector3(transform.position.x - publicSpeed * Time.deltaTime, transform.position.y, transform.position.z);
                break;
            case FoxDirection.right:
                transform.position = new Vector3(transform.position.x + publicSpeed * Time.deltaTime, transform.position.y, transform.position.z);
                break;
        }
    }

    public void changeDirection(FoxDirection direction)
    {
        _privateTurning = true;
        _privateNewFoxDirection = direction;
    }

 

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Target"))
        {
            //Debug.Log("Victory");
            LevelManager.Instance.CurrentGameState = GameState.FINISH;
            ChickenLeftOnMap = GameObject.FindGameObjectsWithTag("Chicken").Length;
            collision.GetComponent<ParticleSystem>().Play();
            collision.GetComponent<SpriteRenderer>().enabled = false;
            Destroy(collision.gameObject, 0.5f);
            LevelManager.Instance.ShowEndLevelUI();
            CurrentLevel.LevelNumber++;
            if (CurrentLevel.LevelNumber > PlayerPrefs.GetInt("LastLevel")) {
                PlayerPrefs.SetInt("LastLevel", CurrentLevel.LevelNumber);
            }
        }
        if (collision.CompareTag("Chicken"))
        {
          //Debug.Log("zjadlo kurczaka ");
          eatenChickens++;
          CurrentPower += PointsForChicken;
          collision.GetComponent<ParticleSystem>().Play();
          collision.GetComponent<SpriteRenderer>().enabled = false;
          Destroy(collision.gameObject,0.5f);
        }
        if(collision.CompareTag("Turn"))
        {
            //Debug.Log("Turn "+collision.name);
            _privateStrzalka = collision;
            changeDirection(collision.GetComponent<TurnScript>().publicDirection);
            _privateMoveToCenter = true;
            _privateCenterPosition = collision.transform.position;
        }        
    }

    public void UpdateDirection()
    {
        switch (publicDirection)
        {
            case FoxDirection.top:
                /*transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0f, 0f, 0f)), publicTurningSpeed);
                if (transform.rotation.ToEuler().z < 5f && transform.rotation.ToEuler().z > -5f)
                {*/
            //transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
            // _privateTurning = false;
            _privateAnimator.SetTrigger("Top");
                _privateSpriteRenderer.flipX = false;
                //}
                break;
            case FoxDirection.bottom:
                /*transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0f, 0f, 180f)), publicTurningSpeed);
                if (transform.rotation.ToEuler().z < 185f && transform.rotation.ToEuler().z > 175f)
                {*/
                //transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 180f));
                //_privateTurning = false;
                _privateAnimator.SetTrigger("Bottom");
                _privateSpriteRenderer.flipX = false;
                //}

                break;
            case FoxDirection.left:
                /*transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0f, 0f, 90f)), publicTurningSpeed);
                if (transform.rotation.ToEuler().z < 95f && transform.rotation.ToEuler().z > 85f)
                {*/
                //transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 90f));
                //_privateTurning = false;
                _privateAnimator.SetTrigger("Left");
                _privateSpriteRenderer.flipX = false;
                //}
                break;
            case FoxDirection.right:
                /*transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0f, 0f, 270f)), publicTurningSpeed);
                if (transform.rotation.ToEuler().z < 275f && transform.rotation.ToEuler().z > 265f)
                {*/
                //transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 270f));
                //_privateTurning = false;
                _privateAnimator.SetTrigger("Right");
                _privateSpriteRenderer.flipX = true;
                //}
                break;
        }
    }

  public float GetCurrentPower01()
  {
    return Mathf.InverseLerp(0, StartPower, CurrentPower);

  }

}
