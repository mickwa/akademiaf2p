﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ItemsLoader
{
  public static GameObject LoadResource(CardTypes itemType)
  {
    GameObject item = null;
    switch (itemType)
    {
      case (CardTypes.DIRECTION_UP):
        {
          item = Resources.Load<GameObject>("Items/TurnObject_UP");
          break;
        }

      case (CardTypes.DIRECTION_DOWN):
        {
          item = Resources.Load<GameObject>("Items/TurnObject_DOWN");
          break;
        }

      case (CardTypes.DIRECTION_LEFT):
        {
          item = Resources.Load<GameObject>("Items/TurnObject_LEFT");
          break;
        }

      case (CardTypes.DIRECTION_RIGHT):
        {
          item = Resources.Load<GameObject>("Items/TurnObject_RIGHT");
          break;
        }

      case (CardTypes.BONE):
        {
          item = Resources.Load<GameObject>("Items/Bone");
          item.name = "Bone";
          break;
        }

      case (CardTypes.SEEDS):
        {
          item = Resources.Load<GameObject>("Items/Seed");
          item.name = "Seed";
          break;
        }

        // Patch creates no object, it replaces hole in earth.
    }
    if (item == null)
      Debug.Log("Unsupported item: " + itemType);
    return item;
  }
}