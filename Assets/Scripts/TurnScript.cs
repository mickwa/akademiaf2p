﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnScript : MonoBehaviour {


    public FoxDirection publicDirection ;
    public GameObject tapIcon;

    private void Start()
    {
        switch (publicDirection)
        {
            case FoxDirection.top:
                transform.rotation = Quaternion.Euler(0f, 0f, 0f);
                break;
            case FoxDirection.right:
                transform.rotation = Quaternion.Euler(0f, 0f, 270f);
                break;
            case FoxDirection.bottom:
                transform.rotation = Quaternion.Euler(0f, 0f, 180f);
                break;
            case FoxDirection.left:
                transform.rotation = Quaternion.Euler(0f, 0f, 90f);
                break;
        }
        if(PlayerPrefs.GetInt("TutorialPassed") != 1) 
        {
            tapIcon.SetActive(true);
            UIController.Instance.TutorialNextStep();
        }
    }

    public void changeDirection()
    {
        switch(publicDirection)
        {
            case FoxDirection.top:
                publicDirection = FoxDirection.right;
                transform.rotation = Quaternion.Euler(0f, 0f, 270f);
                break;
            case FoxDirection.right:
                publicDirection = FoxDirection.bottom;
                transform.rotation = Quaternion.Euler(0f, 0f, 180f);
                break;
            case FoxDirection.bottom:
                publicDirection = FoxDirection.left;
                transform.rotation = Quaternion.Euler(0f, 0f, 90f);
                break;
            case FoxDirection.left:
                publicDirection = FoxDirection.top;
                transform.rotation = Quaternion.Euler(0f, 0f, 0f);
                break;
        }
    }

    private void OnMouseDown()
    {
        if(tapIcon.active) 
        {
            if (LevelManager.Instance.CurrentGameState == GameState.TUTORIAL) 
            {
                PlayerPrefs.SetInt("TutorialPassed", 1);
                LevelManager.Instance.CurrentGameState = GameState.GAMEPLAY;
                tapIcon.SetActive(false);
                UIController.Instance.hideTutorialInfo();
                
            }
        }
        
        changeDirection();
    }
    private void OnMouseOver()
    {
        
    }
}
