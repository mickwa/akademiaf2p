﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToSeedScript : MonoBehaviour {

    [HideInInspector]
    public Vector3 publicDestination;
    private HenScript _privateHS;

    // Use this for initialization
    void Start()
    {
        _privateHS = GetComponent<HenScript>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, publicDestination, _privateHS.publicMovementSpeed * Time.deltaTime);
    }
}
