﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialAnimationScript : MonoBehaviour {

    public Animator PublicAnimator;
    public TextMesh PublicText;

	// Use this for initialization
	void Start () {
        PublicAnimator.speed = 0;
        if (PlayerPrefs.GetInt("TutorialPassed")!=1)
        {

        }
	}
	public void StartTutorial()
    {
        
        PublicText.text = "Drag Card";
        PublicAnimator.speed = 1;
    }
}
