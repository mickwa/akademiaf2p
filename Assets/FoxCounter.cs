﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FoxCounter : MonoBehaviour
{ 
  FoxScript Fox;
  TextMeshProUGUI label;
  void Start()
  {
    label = GetComponent<TextMeshProUGUI>();
  }

  private void RegisterEvents()
  {
    Fox.OnFoxPowerChanged += Fox_OnFoxPowerChanged;
  }

  private void Fox_OnFoxPowerChanged()
  { 
    label.text = Fox.CurrentPower.ToString();
  }

  private void UnregisterEvents()
  {
   if(Fox!=null)
    Fox.OnFoxPowerChanged -= Fox_OnFoxPowerChanged;
  }

  void Update()
  {
    if (Fox == null)
    {
      Fox = FindObjectOfType<FoxScript>();
      if (Fox != null)
      {
        Fox_OnFoxPowerChanged();
        Debug.Log("Found fox for counter: " + name);
        RegisterEvents();
      }
    }
  }
     

  void OnDestroy()
  {
    UnregisterEvents();
  }
}
